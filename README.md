Compile.py
==================
Because there just weren't enough static site builders.

Don't use this one, it's probably too basic for most needs. This documentation is mostly for me.


Usage
==================
Configure a directory with the following structure:

```
Site
|
+--config.py
|
+--src
|  |
|  +--pages
|  |  |
|  |  +--resources/<resources shared by all 'pages'>
|  |  |
|  |  +--about.md
|  |
|  +--posts
|     |
|     +--2018-01-01_some-post-title
|        |
|        +--resources/<individual post resources>
|        |
|        +--content.md
|
+--theme
   |
   +--static/<static css/js here>
   |
   +--templates
      |
      +--archive.html
      |
      +--index.html
      |
      +--page.html
      |
      +--post.html
```

Install the script (python 3.5+ required) and run:

```
pip install -e git+https://gitlab.com/mirkovu/compile.py#egg=compile.py
# Navigate to your directory
compile.py
```
