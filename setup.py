from setuptools import setup

setup(
    name='compile.py',
    version='0.0.1',
    python_requires='>=3.5',
    description='A poorly made static site builder.',
    url='http://gitlab.com/mirkovu/config.py',
    author='Mirko Vucicevich',
    author_email='mirkovu@gmail.com',
    license='Unlicense',
    install_requires=[
        'Jinja2==2.10',
        'Markdown==3.0.1'
    ],
    scripts=[
        'bin/compile.py'
    ],
    zip_safe=False
)
