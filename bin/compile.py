#! /usr/bin/env python

from jinja2 import Environment, FileSystemLoader, select_autoescape
import markdown
from pathlib import Path, PurePath
from collections import defaultdict
import os
import sys
from importlib import import_module
import shutil

class add_sys_path():
    '''Temporarily adds a sys path. Used in this case to allow importing
    variables from a local folder's config.py.

    Taken from https://stackoverflow.com/a/39855753
    '''
    def __init__(self, path):
        self.path = path

    def __enter__(self):
        sys.path.insert(0, self.path)

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            sys.path.remove(self.path)
        except ValueError:
            pass


# Configuration --------------------------------------------------------------
config = {
    'SITE_NAME': 'Site',
    'BASE_URL': '/',
    'TEMPLATE_DIR': './theme/templates',
    'STATIC_DIR': './theme/static',
    'SOURCE_DIR': './src',
    'OUTPUT_DIR': './output',
    'MARKDOWN_EXTENSIONS': []
}

with add_sys_path('.'):
    import config as local_config
    for key in config.keys():
        if hasattr(local_config, key):
            config[key] = getattr(local_config, key)

env = Environment(
    loader=FileSystemLoader(config['TEMPLATE_DIR']),
    autoescape=select_autoescape(['html', 'xml'])
)

md = markdown.Markdown(extensions=['meta']+config['MARKDOWN_EXTENSIONS'])


def getMDFiles(root):
    return [x for x in Path(root).glob('**/*.md')]


def parseMD(file):
    with open(file, 'r') as f:
        html = md.convert(f.read())
    # Turn single-item arrays into the items themselves
    return (
        html,
        {key: val[0] if len(val) == 1 else val for key, val in md.Meta.items()}
    )


def copyTree(src, dst, symlinks=False, ignore=None):
    '''Recursive folder copy.
    Need to remove this to remove dependency on os(prefer to use pathlib)

    From https://stackoverflow.com/a/12514470
    '''
    for item in os.listdir(str(src)):
        s = os.path.join(str(src), item)
        d = os.path.join(str(dst), item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)


def prepareOutput():
    '''Cleans up existing output and initializes directories'''
    if Path(config['OUTPUT_DIR']).is_dir():
        # Delete old output if exists
        shutil.rmtree(config['OUTPUT_DIR'])

    output_static = Path(PurePath(config['OUTPUT_DIR'], 'static'))
    output_static.mkdir(parents=True, exist_ok=True)
    if Path(config['STATIC_DIR']).is_dir():
        # Copy static files if any
        copyTree(config['STATIC_DIR'], output_static)


def collectPages():
    resources = Path(PurePath(config['SOURCE_DIR'], 'pages', 'resources'))
    output_dir = PurePath(config['OUTPUT_DIR'], 'pages')
    output_resources = Path(output_dir.joinpath('resources'))

    # Make directories
    output_resources.mkdir(parents=True, exist_ok=True)

    # Copy resources...
    if resources.is_dir():
        copyTree(resources, output_resources)

    for page in getMDFiles(os.path.join(config['SOURCE_DIR'], 'pages')):
        filename = page.with_suffix('.html').name
        output = Path(PurePath(output_dir, filename))
        body, meta = parseMD(str(page))
        yield {
            'title': page.with_suffix('').name,
            'body': body,
            'meta': meta,
            'url': config['BASE_URL'] + 'pages/' + filename,
            'output': output,
            'template': 'page.html',
        }


def collectPosts():
    post_source = PurePath(config['SOURCE_DIR'], 'posts')
    for post in getMDFiles(post_source):
        resources = Path(PurePath(*post.parts[:-1], 'resources'))
        output_dir = PurePath('./output', *post.parts[1:-1])
        output_resources = Path(output_dir.joinpath('resources'))

        # Make directories
        output_resources.mkdir(parents=True, exist_ok=True)

        # Copy resources...
        if resources.is_dir():
            copyTree(resources, output_resources)
        body, meta = parseMD(str(post))
        yield {
            'body': body,
            'meta': meta,
            'url': '{}posts/{}/'.format(config['BASE_URL'], post.parts[-2]),
            'output': Path(output_dir.joinpath('index.html')),
            'template': 'post.html'
        }


def collectCategories(posts):
    categories = defaultdict(list)
    for post in posts:
        for category in post.get('categories', []):
            categories[category].append(post)
    return [
        {'title': key, 'posts': val, 'template': 'category.html'}
        for key, val in categories.items()
    ]


def collectEverything():
    collection = {}
    collection['posts'] = [x for x in collectPosts()]
    collection['pages'] = [x for x in collectPages()]
    collection['categories'] = collectCategories(collection['posts'])
    collection['other'] = [
        {'output': Path('./output/index.html'),
         'template': 'index.html',
         'title': 'Home'},
        {'output': Path('./output/posts/index.html'),
         'template': 'archive.html',
         'title': 'Archive'}
    ]
    return collection


def compileItem(item, collection):
    template = env.get_template(item['template'])

    if 'output' in item:
        with item['output'].open('w') as output:
            output.write(
                template.render(
                    this=item,
                    base_url=config['BASE_URL'],
                    **collection
                )
            )


def compileCollection(collection):
    for col in collection.values():
        for x in col:
            compileItem(x, collection)


prepareOutput()
compileCollection(collectEverything())
